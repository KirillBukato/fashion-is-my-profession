//
// Created by savchik on 28/05/24.
//

#include "GeneralDataEntity.h"

GeneralDataEntity::GeneralDataEntity(const QSqlRecord &record) : AbstractEntity(record) {

    if (record.contains("age")) {
        general_data_.age = record.field("age").value().toInt();
    }
    if (record.contains("face_shape")) {
        general_data_.face_shape = record.field("face_shape").value().toString();
    }
    if (record.contains("eye_shape")) {
        general_data_.eye_shape = record.field("eye_shape").value().toString();
    }
    if (record.contains("face_features")) {
        general_data_.face_features = record.field("face_features").value().toString();
    }
    if (record.contains("body_features")) {
        general_data_.body_features = record.field("body_features").value().toString();
    }
    if (record.contains("social_status")) {
        general_data_.social_status = record.field("social_status").value().toString();
    }


}

GeneralData &GeneralDataEntity::GetGeneralData() {
    return general_data_;
}
